# Bulka Page

## Setup:

- Install SASS globally: `npm i -g sass`
- `source build-sass.sh`

## Notes:

- Menu uses flexbox because **flexbox is created for such things**. Using grid here looks a bit strange for me.
